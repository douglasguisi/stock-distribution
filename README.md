# Teste UBS

Este projeto é a implementação do teste "Ler, Armazenar, Calcular dados" para o cliente UBS

## Observações sobre a solução adotada

* Parte 1: Ler e Armazenar
	- Para leitura, serão lidos todos os arquivos que estiverem na pasta "massa" no diretório raiz do projeto.
	- Para o armazenamento, foi utilizado uma instância de banco de dados H2 que é iniciada junto com o Spring Boot, e armazena as informações em filesystem (diretório ~/.h2/testeubs).
	- Não houve validação de registros duplicados por arquivo por não existir uma definição de quais campos considerar para esta validação.

* Parte 2: Calcular dados
	- A lógica de distribuição criada considerou que houvesse a distribuição mais justa possível **por transação**, ou seja, para cada transação, a distribuição entre as lojas deve ser a mais parecida possível.

## Executando a aplicação

Abaixo seguem as instruções para executar a aplicação em ambiente local.

### Pré requisitos

* Git
* JDK8
* Maven 3+
* SonarQube (opcional, caso queira gerar relatório de qualidade de código - porta 9000)


### Passo-a-passo

1) Faça o clone do projeto para seu repositório local.

2) Através de um terminal de linha de comando, no diretório raiz do projeto, execute:


	mvn clean package

	java -jar target/app.jar
	
3) No output da aplicação deverá ser possível verificar que, após a inicialização do Spring Boot, os arquivos de dados foram carregados com sucesso.

4) Exemplo de consulta do cálculo de distribuição: [link](http://localhost:8040/stock-distribution/stockDistribution?product=EMMS&buyersNumber=2)

## Gerando relatório no SonarQube

1) Através de um terminal de linha de comando, no diretório raiz do projeto, execute:


	mvn clean package jacoco:report
	
	mvn test sonar:sonar

Acesse o console de administração do SonarQube (por padrão em http://localhost:9000) para visualizar o relatório. 

## Autores

* **Douglas Guisi**