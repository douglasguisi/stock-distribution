package com.ubs.stockdistribution.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import com.ubs.stockdistribution.dto.BuyerDistributionDTO;
import com.ubs.stockdistribution.dto.BuyerDistributionItemDTO;
import com.ubs.stockdistribution.dto.StockDistributionDTO;
import com.ubs.stockdistribution.entity.StockTransaction;
import com.ubs.stockdistribution.enums.StockTransactionType;
import com.ubs.stockdistribution.exception.BusinessException;

@RunWith(SpringRunner.class)
public class StockDistributionServiceTest {

	@InjectMocks
	private StockDistributionService stockDistributionService;
	
	@Mock
	private StockTransactionService stockTransactionService;
	
	private List<StockTransaction> stockTransactions;
	private StockDistributionDTO stockDistributionDTO;
	
	@Before
	public void init() {
		this.stockDistributionDTO = new StockDistributionDTO();
		this.stockDistributionDTO.setAveragePrice(BigDecimal.valueOf(5.39));
		this.stockDistributionDTO.setProduct("EMMS");
		this.stockDistributionDTO.setQuantity(36);
		this.stockDistributionDTO.setVolume(BigDecimal.valueOf(194.04));
		
		this.stockTransactions = new ArrayList<>();
		
		StockTransaction stockTransaction = new StockTransaction();
		stockTransaction.setId(1);
		stockTransaction.setIndustry("industry");
		stockTransaction.setOrigin("origin");
		stockTransaction.setTransactionType(StockTransactionType.BUY);
		stockTransaction.setType("type");
		stockTransaction.setProduct("EMMS");
		stockTransaction.setQuantity(36);
		stockTransaction.setPrice(BigDecimal.valueOf(5.39));
		this.stockTransactions.add(stockTransaction);
		
		this.stockDistributionDTO.setBuyerDistributions(new ArrayList<>());
		
		BuyerDistributionDTO buyerDistribution = new BuyerDistributionDTO();
		buyerDistribution.setBuyerName("Lojista 1");
		buyerDistribution.setQuantity(18);
		buyerDistribution.setVolume(BigDecimal.valueOf(97.02));
		buyerDistribution.setAveragePrice(BigDecimal.valueOf(5.39).setScale(4));
		buyerDistribution.setItems(new ArrayList<>());
		buyerDistribution.addItem(new BuyerDistributionItemDTO(1, 18, BigDecimal.valueOf(5.39)));
		this.stockDistributionDTO.getBuyerDistributions().add(buyerDistribution);
		
		buyerDistribution = new BuyerDistributionDTO();
		buyerDistribution.setBuyerName("Lojista 2");
		buyerDistribution.setQuantity(18);
		buyerDistribution.setVolume(BigDecimal.valueOf(97.02));
		buyerDistribution.setAveragePrice(BigDecimal.valueOf(5.39).setScale(4));
		buyerDistribution.setItems(new ArrayList<>());
		buyerDistribution.addItem(new BuyerDistributionItemDTO(1, 18, BigDecimal.valueOf(5.39)));
		this.stockDistributionDTO.getBuyerDistributions().add(buyerDistribution);
	}
	
	@Test
	public void whenCalculateStockDistributionThenReturnStockDistribution() {
		when(this.stockTransactionService.findAllByProduct(ArgumentMatchers.anyString())).thenReturn(this.stockTransactions);

		StockDistributionDTO retorno = this.stockDistributionService.calculateStockDistribution("EMMS", 2);
		
		assertThat(retorno).usingRecursiveComparison().isEqualTo(this.stockDistributionDTO);
	}
	
	@Test
	public void whenCalculateStockDistributionWithRemainingThenReturnStockDistribution() {
		this.stockTransactions.get(0).setQuantity(37);
		this.stockDistributionDTO.setQuantity(37);
		this.stockDistributionDTO.setVolume(BigDecimal.valueOf(199.43));
		
		when(this.stockTransactionService.findAllByProduct(ArgumentMatchers.anyString())).thenReturn(this.stockTransactions);

		StockDistributionDTO retorno = this.stockDistributionService.calculateStockDistribution("EMMS", 2);
		
		assertThat(retorno).isEqualToIgnoringGivenFields(this.stockDistributionDTO, "buyerDistributions");
	}
	
	@Test(expected = BusinessException.class)
	public void whenCalculateStockDistributionThenThrowBusinessException() {
		when(this.stockTransactionService.findAllByProduct(ArgumentMatchers.anyString())).thenReturn(Collections.emptyList());
		
		this.stockDistributionService.calculateStockDistribution("EMMS", 2);
	}
	
}
