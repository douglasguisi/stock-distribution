package com.ubs.stockdistribution.service;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import com.ubs.stockdistribution.dto.StockTransactionListDTO;
import com.ubs.stockdistribution.entity.ProcessedFile;
import com.ubs.stockdistribution.enums.ProcessedFileType;

@RunWith(SpringRunner.class)
public class StockTransactionLoadServiceTest {

	@InjectMocks
	private StockTransactionLoadService stockTransactionLoadService;
	
	@Mock
	private ProcessedFileService processedFileService;
	
	@Mock
	private StockTransactionService stockTransactionService;
	
	@Mock
	private StockTransactionLoadService self;
	
	private ProcessedFile processedFile;
	
	@Before
	public void init() {
		this.processedFile = new ProcessedFile();
		this.processedFile.setFileName("data_1.json");
		this.processedFile.setType(ProcessedFileType.STOCK_TRANSACTION);
	}
	
	@Test
	public void whenLoadBuyStockTransactionsThenSuccess() {
		when(this.processedFileService.findAllByType(ProcessedFileType.STOCK_TRANSACTION)).thenReturn(Arrays.asList(this.processedFile));

		this.stockTransactionLoadService.loadBuyStockTransactions();
		
		assertTrue(Boolean.TRUE);
	}
	
	@Test
	public void whenLoadBuyStockTransactionsFileAlreadyProcessedThenSuccess() {
		when(this.processedFileService.findAllByType(ProcessedFileType.STOCK_TRANSACTION)).thenReturn(Arrays.asList(this.processedFile));

		this.stockTransactionLoadService.loadBuyStockTransactions();
		
		assertTrue(Boolean.TRUE);
	}
	
	@Test
	public void whenSaveStockTransactionsThenSuccess() {
		this.stockTransactionLoadService.saveStockTransactions("fileName", new StockTransactionListDTO());
		
		assertTrue(Boolean.TRUE);
	}
	
}
