package com.ubs.stockdistribution.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import com.ubs.stockdistribution.entity.ProcessedFile;
import com.ubs.stockdistribution.enums.ProcessedFileType;
import com.ubs.stockdistribution.repository.ProcessedFileRepository;

@RunWith(SpringRunner.class)
public class ProcessedFileServiceTest {

	@InjectMocks
	private ProcessedFileService processedFileService;
	
	@Mock
	private ProcessedFileRepository processedFileRepository;
	
	private ProcessedFile processedFile;

	@Before
	public void init() {
		this.processedFile = new ProcessedFile();
		this.processedFile.setFileName("filename");
		this.processedFile.setType(ProcessedFileType.STOCK_TRANSACTION);
	}
	
	@Test
	public void whenSaveProcessedFileThenReturnProcessedFile() {
		when(this.processedFileRepository.save(ArgumentMatchers.any())).thenReturn(this.processedFile);

		ProcessedFile retorno = this.processedFileService.saveProcessedFile("filename", ProcessedFileType.STOCK_TRANSACTION);
		
		assertThat(retorno).usingRecursiveComparison().isEqualTo(this.processedFile);
	}
	
	@Test
	public void whenFindAllByTypeThenReturnProcessedFileList() {
		List<ProcessedFile> processedFiles = Arrays.asList(this.processedFile);
		when(this.processedFileRepository.findAllByType(ArgumentMatchers.any())).thenReturn(processedFiles);

		List<ProcessedFile> retorno = this.processedFileService.findAllByType(ProcessedFileType.STOCK_TRANSACTION);
		
		assertThat(retorno).usingRecursiveComparison().isEqualTo(processedFiles);
	}
	
	
}
