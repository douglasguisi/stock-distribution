package com.ubs.stockdistribution.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.test.context.junit4.SpringRunner;

import com.ubs.stockdistribution.dto.StockTransactionDTO;
import com.ubs.stockdistribution.dto.StockTransactionListDTO;
import com.ubs.stockdistribution.entity.StockTransaction;
import com.ubs.stockdistribution.enums.StockTransactionType;
import com.ubs.stockdistribution.repository.StockTransactionRepository;

@RunWith(SpringRunner.class)
public class StockTransactionServiceTest {

	@InjectMocks
	private StockTransactionService stockTransactionService;

	@Mock
	private StockTransactionRepository stockTransactionRepository;

	@Mock
	private ModelMapper modelMapper;

	private StockTransaction stockTransaction;
	private StockTransactionDTO stockTransactionDTO;

	@Before
	public void initStockTransaction() {
		this.stockTransaction = new StockTransaction();
		this.stockTransaction.setId(1);
		this.stockTransaction.setIndustry("industry");
		this.stockTransaction.setOrigin("origin");
		this.stockTransaction.setTransactionType(StockTransactionType.BUY);
		this.stockTransaction.setType("type");
		this.stockTransaction.setProduct("EMMS");
		this.stockTransaction.setQuantity(36);
		this.stockTransaction.setPrice(BigDecimal.valueOf(5.39));
	}

	@Before
	public void initStockTransactionDTO() {
		this.stockTransactionDTO = new StockTransactionDTO();
		this.stockTransactionDTO.setIndustry("industry");
		this.stockTransactionDTO.setOrigin("origin");
		this.stockTransactionDTO.setTransactionType(StockTransactionType.BUY);
		this.stockTransactionDTO.setType("type");
		this.stockTransactionDTO.setProduct("EMMS");
		this.stockTransactionDTO.setQuantity(36);
		this.stockTransactionDTO.setPrice(BigDecimal.valueOf(5.39));
	}

	@Test
	public void whenFindAllByProductThenReturnStockTransactionList() {
		List<StockTransaction> stockTransactions = Arrays.asList(this.stockTransaction);
		when(this.stockTransactionRepository.findAllByProduct(ArgumentMatchers.any())).thenReturn(stockTransactions);

		List<StockTransaction> retorno = this.stockTransactionService.findAllByProduct("EMMS");

		assertThat(retorno).usingRecursiveComparison().isEqualTo(stockTransactions);
	}

	@Test
	public void whenSaveStockTransactionsThenReturnStockTransactionList() {
		StockTransactionListDTO dto = new StockTransactionListDTO();
		dto.setData(Arrays.asList(this.stockTransactionDTO));

		when(this.modelMapper.map(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(this.stockTransaction);

		StockTransactionListDTO retorno = this.stockTransactionService.saveStockTransactions(dto);

		assertThat(retorno).usingRecursiveComparison().isEqualTo(dto);
	}

	@Test
	public void whenSaveStockTransactionsDTONullThenReturnNull() {
		StockTransactionListDTO retorno = this.stockTransactionService.saveStockTransactions(null);

		assertThat(retorno).isNull();
	}

	@Test
	public void whenSaveStockTransactionsListNullThenReturnDTODataNull() {
		StockTransactionListDTO dto = new StockTransactionListDTO();

		StockTransactionListDTO retorno = this.stockTransactionService.saveStockTransactions(dto);

		assertThat(retorno).usingRecursiveComparison().isEqualTo(dto);
	}

}
