package com.ubs.stockdistribution.api;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ubs.stockdistribution.dto.StockDistributionDTO;
import com.ubs.stockdistribution.service.StockDistributionService;

@RunWith(SpringRunner.class)
@WebMvcTest(StockDistributionController.class)
public class StockDistributionControllerTest {

	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private StockDistributionService stockDistributionService;
	
	private StockDistributionDTO stockDistributionDTO;

	@Before
	public void init() {
		this.stockDistributionDTO = new StockDistributionDTO();
		this.stockDistributionDTO.setAveragePrice(BigDecimal.valueOf(123));
		this.stockDistributionDTO.setProduct("product");
		this.stockDistributionDTO.setQuantity(5);
		this.stockDistributionDTO.setVolume(BigDecimal.valueOf(456));
		this.stockDistributionDTO.setBuyerDistributions(new ArrayList<>());
	}
	
	@Test
	public void whenCalculateStockDistributionThenReturnStockDistribution() throws Exception {
		when(this.stockDistributionService.calculateStockDistribution(ArgumentMatchers.anyString(), ArgumentMatchers.anyInt())).thenReturn(this.stockDistributionDTO);

		MockHttpServletRequestBuilder getPath = get("/stockDistribution?product=product&buyersNumber=2");
		
		MvcResult result = this.mvc.perform(getPath).andExpect(status().isOk()).andReturn();
		
		StockDistributionDTO retorno = new ObjectMapper().readValue(result.getResponse().getContentAsString(StandardCharsets.UTF_8), StockDistributionDTO.class);
		
		assertThat(retorno).usingRecursiveComparison().isEqualTo(this.stockDistributionDTO);
	}
	
}
