package com.ubs.stockdistribution.exception;

import static org.assertj.core.api.Assertions.assertThat;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;

import com.ubs.stockdistribution.dto.ErrorDTO;
import com.ubs.stockdistribution.mock.BindingResultMock;
import com.ubs.stockdistribution.mock.ConstraintViolationMock;

@RunWith(SpringRunner.class)
public class GlobalExceptionHandlerTest {
	
	@Autowired
	private GlobalExceptionHandler globalExceptionHandler;

	@Test
	public void quandoHandleConstraintViolationEntaoRetorneMensagemErro() throws Exception {
		
		String errorMessage = "Mensagem de erro";
		
		ConstraintViolationMock constraint = new ConstraintViolationMock(errorMessage);
		
		ConstraintViolationException ex = new ConstraintViolationException(Stream.of(constraint).collect(Collectors.toSet()));
		
		ResponseEntity<Object> retornoEntity = globalExceptionHandler.handleConstraintViolation(ex, null);
		
		assertThat(retornoEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
		assertThat(retornoEntity.getBody()).isInstanceOf(ErrorDTO.class);
		
		ErrorDTO retorno = (ErrorDTO) retornoEntity.getBody();
		assertThat(retorno.getCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
		assertThat(retorno.getMessage()).isEqualToIgnoringCase(errorMessage);
	}
	
	@Test
	public void quandoHandleConflictEntaoErrorDTOMensagemErro() throws Exception {
		
		String errorMessage = "Mensagem de erro";
		
		ResponseEntity<Object> retornoEntity = globalExceptionHandler.handleConflict(new RuntimeException(errorMessage), null);
		
		assertThat(retornoEntity.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
		assertThat(retornoEntity.getBody()).isInstanceOf(ErrorDTO.class);
		
		ErrorDTO retorno = (ErrorDTO) retornoEntity.getBody();
		assertThat(retorno.getCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR.value());
		assertThat(retorno.getMessage()).isEqualToIgnoringCase(errorMessage);
	}
	
	@Test
	public void quandoHandleMethodArgumentNotValidEntaoRetorneMensagemErro() throws Exception {
		
		String errorMessage = "Mensagem de erro";
		BindingResultMock bindingResult = new BindingResultMock(Arrays.asList(new ObjectError("Erro", errorMessage)));
		
		ResponseEntity<Object> retornoEntity = globalExceptionHandler.handleMethodArgumentNotValid(new MethodArgumentNotValidException(null, bindingResult), null, null, null);
		
		assertThat(retornoEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
		assertThat(retornoEntity.getBody()).isInstanceOf(ErrorDTO.class);
		
		ErrorDTO retorno = (ErrorDTO) retornoEntity.getBody();
		assertThat(retorno.getCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
		assertThat(retorno.getMessage()).isEqualToIgnoringCase(errorMessage);
	}
	
	@Test
	public void quandoHandleIllegalArgumentEntaoRetorneMensagemErro() throws Exception {
		
		String errorMessage = "Mensagem de erro";
		
		IllegalArgumentException ex = new IllegalArgumentException(errorMessage);
		
		ResponseEntity<Object> retornoEntity = globalExceptionHandler.handleIllegalArgument(ex, null);
		
		assertThat(retornoEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
		assertThat(retornoEntity.getBody()).isInstanceOf(ErrorDTO.class);
		
		ErrorDTO retorno = (ErrorDTO) retornoEntity.getBody();
		assertThat(retorno.getCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
		assertThat(retorno.getMessage()).isEqualToIgnoringCase(errorMessage);
	}
	
	@Test
	public void quandoHandleMissingServletRequestParameterEntaoRetorneMensagemErro() throws Exception {
		
		String errorMessage = "O campo {0} é obrigatório";
		String nomeCampo = "Nome Campo";
		
		MissingServletRequestParameterException ex = new MissingServletRequestParameterException(nomeCampo, "String");
		
		ResponseEntity<Object> retornoEntity = globalExceptionHandler.handleMissingServletRequestParameter(ex, null, null, null);
		assertThat(retornoEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
		assertThat(retornoEntity.getBody()).isInstanceOf(ErrorDTO.class);

		ErrorDTO retorno = (ErrorDTO) retornoEntity.getBody();
		assertThat(retorno.getCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
		assertThat(retorno.getMessage()).isEqualToIgnoringCase(MessageFormat.format(errorMessage, ex.getParameterName()));
	}
	
	@Test
	public void quandoHandleApiExceptionEntaoRetorneMensagemErro() throws Exception {
		
		String errorMessage = "Mensagem de erro";
		
		BusinessException ex = new BusinessException(errorMessage);
		
		ResponseEntity<Object> retornoEntity = globalExceptionHandler.handleApiException(ex, null);
		
		assertThat(retornoEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
		assertThat(retornoEntity.getBody()).isInstanceOf(ErrorDTO.class);
		
		ErrorDTO retorno = (ErrorDTO) retornoEntity.getBody();
		assertThat(retorno.getCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
		assertThat(retorno.getMessage()).isEqualToIgnoringCase(errorMessage);
	}
	
	@Configuration
	public static class Config {
		
		@Bean
	    public GlobalExceptionHandler globalExceptionHandler() {
	        return new GlobalExceptionHandler();
	    }
	}
}