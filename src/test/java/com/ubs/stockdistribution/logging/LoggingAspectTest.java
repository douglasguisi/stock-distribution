package com.ubs.stockdistribution.logging;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class LoggingAspectTest {

	@InjectMocks
	private LoggingAspect loggingAspect;

	@Before
	public void init() {
	}

	@Test
	public void whenMethodTimeLoggerThenReturnObject() throws Throwable {
		ProceedingJoinPoint joinPoint = Mockito.mock(ProceedingJoinPoint.class);
		MethodSignature methodSignature = Mockito.mock(MethodSignature.class);

		Object obj = new Object();
		
		when(joinPoint.getSignature()).thenReturn(methodSignature);
		when(methodSignature.getDeclaringType()).thenReturn(LoggingAspectTest.class);
		when(methodSignature.getName()).thenReturn("methodName");
		when(joinPoint.proceed()).thenReturn(obj);

		Object retorno = this.loggingAspect.methodTimeLogger(joinPoint);

		assertThat(retorno).isEqualTo(obj);
	}

}
