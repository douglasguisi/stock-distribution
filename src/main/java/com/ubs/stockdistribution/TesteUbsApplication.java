package com.ubs.stockdistribution;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesteUbsApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesteUbsApplication.class, args);
	}

}
