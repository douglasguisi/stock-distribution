package com.ubs.stockdistribution.enums;

/**
 * Contém o domínio dos tipos de transação contemplados no sistema
 * 
 * @author douglas.guisi@gft.com
 *
 */
public enum StockTransactionType {

	BUY, SELL;

}
