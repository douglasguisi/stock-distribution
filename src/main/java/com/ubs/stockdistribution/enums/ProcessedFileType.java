package com.ubs.stockdistribution.enums;

/**
 * Contém o domínio dos tipos de arquivos que podem ser importados no sistema
 * 
 * @author douglas.guisi@gft.com
 *
 */
public enum ProcessedFileType {

	STOCK_TRANSACTION;
}
