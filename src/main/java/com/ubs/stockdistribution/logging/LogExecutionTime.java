package com.ubs.stockdistribution.logging;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Interface utilizada para indicar ao aspect {@link LoggingAspect} para quais métodos deve ser gerado o log de tempo de execução.
 * O método anotado precisa ter o modificador public
 * 
 * @author douglas.guisi@gft.com
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface LogExecutionTime {

}