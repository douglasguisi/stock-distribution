package com.ubs.stockdistribution.logging;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

/**
 * Classe de aspect que intercepta os métodos anotados com {@link LogExecutionTime} e gera o log do tempo de execução dos mesmos
 * 
 * @author douglas.guisi@gft.com
 *
 */
@Aspect
@Component
public class LoggingAspect {

	private static final Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

	@Around("@annotation(com.ubs.stockdistribution.logging.LogExecutionTime)")
	public Object methodTimeLogger(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
		MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();

		String className = methodSignature.getDeclaringType().getSimpleName();
		String methodName = methodSignature.getName();

		StopWatch stopWatch = new StopWatch(className + "." + methodName);
		stopWatch.start(methodName);
		Object result = proceedingJoinPoint.proceed();
		stopWatch.stop();

		if (logger.isInfoEnabled()) {
			logger.info("Executed {}: {}ms", stopWatch.getId(), stopWatch.getLastTaskTimeMillis());
		}
		return result;
	}
}