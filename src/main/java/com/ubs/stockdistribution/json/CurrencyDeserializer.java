package com.ubs.stockdistribution.json;

import java.io.IOException;
import java.math.BigDecimal;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * Deserializer JSON para ignorar símbolos monetários na conversão do valor, mantendo apenas os caracteres que correspondem à expressão regular [^0-9\\.]
 * 
 * @author douglas.guisi@gft.com
 *
 */
public class CurrencyDeserializer extends JsonDeserializer<BigDecimal> {

	@Override
	public BigDecimal deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
		String strValue = p.readValueAsTree().toString();
		BigDecimal value = null;
		if (strValue != null) {
			value = new BigDecimal(strValue.replaceAll("[^0-9\\.]", ""));
		}
		return value;
	}



}