package com.ubs.stockdistribution.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ubs.stockdistribution.dto.StockTransactionListDTO;
import com.ubs.stockdistribution.entity.ProcessedFile;
import com.ubs.stockdistribution.enums.ProcessedFileType;
import com.ubs.stockdistribution.enums.StockTransactionType;
import com.ubs.stockdistribution.logging.LogExecutionTime;

/**
 * Service do Spring para operações referentes à importação de transações de compra ou venda
 * 
 * @author douglas.guisi@gft.com
 *
 */
@Service
public class StockTransactionLoadService {
	
	public static final Logger logger = LoggerFactory.getLogger(StockTransactionLoadService.class);
	
	private static final String INPUT_FILES_LOCATION = "massa";

	@Lazy
	@Autowired
	private StockTransactionLoadService self;
	
	@Autowired
	private ProcessedFileService processedFileService;
	
	@Autowired
	private StockTransactionService stockTransactionService;
	
	/**
	 * Ao concluir o start da aplicação, faz a importação de transações de compra ou venda de acordo com arquivos json que forem disponiibilizados no diretório relativo '\massa',
	 * salvando os registro em banco de dados. Se o arquivo já foi processado anteriormente, é ignorado na importação.
	 */
	@LogExecutionTime
	@EventListener(ApplicationReadyEvent.class)
	public void loadBuyStockTransactions() {
		Stream<Path> files = this.listFiles();
		
		List<ProcessedFile> processedFiles = this.processedFileService.findAllByType(ProcessedFileType.STOCK_TRANSACTION);
		
		files.parallel().forEach(path -> {
			String fileName = path.getFileName().toString();

			if (processedFiles.stream().anyMatch(pf -> StringUtils.equalsIgnoreCase(fileName, pf.getFileName()))) {
				logger.info("O arquivo {} já foi processado.", fileName);
			
			} else {
				byte[] fileContent = this.loadFile(path);
				
				StockTransactionListDTO stockTransactionListDTO = this.readFileContent(fileContent, path.getFileName().toString());
				stockTransactionListDTO.getData().parallelStream().forEach(t -> t.setTransactionType(StockTransactionType.BUY));

				this.self.saveStockTransactions(fileName, stockTransactionListDTO);
			}
		});
	}
	
	/**
	 * Lista arquivos para importação
	 * @return
	 */
	private Stream<Path> listFiles() {
		try {
			return Files.list(Paths.get(INPUT_FILES_LOCATION));
		} catch (IOException e) {
			throw new IllegalStateException("Ocorreu um erro ao ler o diretório " + INPUT_FILES_LOCATION, e);
		}
	}

	/**
	 * Recupera o conteúdo do arquivo
	 * @param path
	 * @return byte[] conteúdo do arquivo
	 */
	private byte[] loadFile(Path path) {
		try {
			return Files.readAllBytes(path);
		} catch (IOException e) {
			throw new IllegalStateException("Ocorreu um erro ao ler o arquivo: " + path.getFileName(), e);
		}
	}
	
	/**
	 * Converte o conteúdo do arquivo para um objeto {@link StockTransactionListDTO}
	 * @param fileContent
	 * @param filename
	 * @return
	 */
	private StockTransactionListDTO readFileContent(byte[] fileContent, String filename) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(fileContent, StockTransactionListDTO.class);
		} catch (IOException e) {
			throw new IllegalStateException("Ocorreu um erro ao ler o conteudo do arquivo: " + filename, e);
		}
	}
	
	/**
	 * Salva as transações e o registro do arquivo importado
	 * @param fileName
	 * @param stockTransactionListDTO
	 */
	@Transactional
	public void saveStockTransactions(String fileName, StockTransactionListDTO stockTransactionListDTO) {
		this.stockTransactionService.saveStockTransactions(stockTransactionListDTO);
		
		this.processedFileService.saveProcessedFile(fileName, ProcessedFileType.STOCK_TRANSACTION);
		
		logger.info("Arquivo {} processado com sucesso.", fileName);
	}
}
