package com.ubs.stockdistribution.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ubs.stockdistribution.dto.StockTransactionListDTO;
import com.ubs.stockdistribution.entity.StockTransaction;
import com.ubs.stockdistribution.repository.StockTransactionRepository;

/**
 * Service do Spring para operações referentes à transações de compra ou venda
 * 
 * @author douglas.guisi@gft.com
 *
 */
@Service
public class StockTransactionService {

	@Autowired
	private StockTransactionRepository stockTransactionRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	public List<StockTransaction> findAllByProduct(String product) {
		return this.stockTransactionRepository.findAllByProduct(product);
	}
	
	@Transactional
	public StockTransactionListDTO saveStockTransactions(StockTransactionListDTO stockTransactionListDTO) {
		if (stockTransactionListDTO != null && stockTransactionListDTO.getData() != null) {
			List<StockTransaction> stockTransactions = Collections.synchronizedList(new ArrayList<>());
			
			stockTransactionListDTO.getData().parallelStream().forEach(dto -> {
				StockTransaction stockTransaction = this.modelMapper.map(dto, StockTransaction.class);
				stockTransactions.add(stockTransaction);
			});
			
			this.stockTransactionRepository.saveAll(stockTransactions);
		}
		return stockTransactionListDTO;
	}
}
