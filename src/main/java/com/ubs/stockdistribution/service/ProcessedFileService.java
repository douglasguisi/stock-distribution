package com.ubs.stockdistribution.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ubs.stockdistribution.entity.ProcessedFile;
import com.ubs.stockdistribution.enums.ProcessedFileType;
import com.ubs.stockdistribution.repository.ProcessedFileRepository;

/**
 * Service do Spring para operações referentes à arquivos importados no sistema
 * 
 * @author douglas.guisi@gft.com
 *
 */
@Service
public class ProcessedFileService {

	@Autowired
	private ProcessedFileRepository processedFileRepository;
	
	@Transactional
	public ProcessedFile saveProcessedFile(String fileName, ProcessedFileType type) {
		ProcessedFile processedFile = new ProcessedFile();
		processedFile.setFileName(fileName);
		processedFile.setType(type);
		return this.processedFileRepository.save(processedFile);
	}
	
	public List<ProcessedFile> findAllByType(ProcessedFileType type) {
		return this.processedFileRepository.findAllByType(type);
	}
}
