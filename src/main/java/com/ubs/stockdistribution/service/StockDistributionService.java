package com.ubs.stockdistribution.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.ubs.stockdistribution.dto.BuyerDistributionDTO;
import com.ubs.stockdistribution.dto.BuyerDistributionItemDTO;
import com.ubs.stockdistribution.dto.StockDistributionDTO;
import com.ubs.stockdistribution.entity.StockTransaction;
import com.ubs.stockdistribution.exception.BusinessException;
import com.ubs.stockdistribution.logging.LogExecutionTime;

/**
 * Service do Spring para operações referentes à distribuição de estoque
 * 
 * @author douglas.guisi@gft.com
 *
 */
@Service
@Validated
public class StockDistributionService {
	
	@Autowired
	private StockTransactionService stockTransactionService;

	@LogExecutionTime
	public StockDistributionDTO calculateStockDistribution(String product,
			@Min(value = 1, message = "A quantidade de lojistas deve ser maior que zero.") Integer buyersNumber) {
		
		List<StockTransaction> transactions = this.findStockTransactionsByProduct(product);
		
		//ordena as transações pelo preço de forma decrescente, para que os últimos itens distribuídos sejam os de menor preço
		transactions.sort(Comparator.comparing(StockTransaction::getPrice).reversed());

		//cria o objeto de distribuição para cada comprador
		List<BuyerDistributionDTO> buyerDistributions = IntStream.range(0, buyersNumber).mapToObj(buyerNumber -> {
			BuyerDistributionDTO buyerDTO = new BuyerDistributionDTO();
			buyerDTO.setBuyerName("Lojista " + (buyerNumber + 1));
			buyerDTO.setItems(Collections.synchronizedList(new ArrayList<>()));
			return buyerDTO;
		}).collect(Collectors.toList());

		/*
		 * Primeiro faz a divisão considerando apenas o coeficiente inteiro, ignorando o resto.
		 * Este processo pode ser paralelo pois aqui a divisão por enquanto é igualitária
		 */
		transactions.parallelStream().forEach(transaction -> {
			int unitsPerBuyer = transaction.getQuantity() / buyersNumber;
			IntStream.range(0, buyersNumber).parallel().forEach(i -> buyerDistributions.get(i)
					.addItem(new BuyerDistributionItemDTO(transaction.getId(), unitsPerBuyer, transaction.getPrice())));
		});
		
		//calcula totais das distribuições
		buyerDistributions.parallelStream().forEach(BuyerDistributionDTO::calculateTotals);
		
		/*
		 * Em seguida, faz a distribuição dos restos da divisão (quando existirem).
		 * Este processo não pode ser paralelo pois a cada iteração precisa considerar os totais para decidir como distribuir os restos
		 */
		for (StockTransaction transaction : transactions) {
			int remaining = transaction.getQuantity() % buyersNumber;
			while (remaining > 0) {
				BuyerDistributionDTO minBuyerDTO = this.getMinValueBuyerDistribution(buyerDistributions);
				minBuyerDTO.incrementItemQuantity(transaction.getId(), 1);
				minBuyerDTO.calculateTotals();
				remaining--;
			}
		}

		StockDistributionDTO dto = new StockDistributionDTO();
		dto.setBuyerDistributions(buyerDistributions);
		dto.calculateTotals();
		dto.setProduct(product);
		
		return dto;
	}
	
	/**
	 * Retorna a distribuição do comprador que possui a menor quantidade de itens, ou se todos tiverem quantidades iguais,
	 * o que tiver o menor valor de volume
	 * @param buyerDistributions
	 * @return {@link BuyerDistributionDTO} distribuição do comprador com a menor quantidade ou menor valor
	 */
	private BuyerDistributionDTO getMinValueBuyerDistribution(List<BuyerDistributionDTO> buyerDistributions) {
		//recupera o comprador que tem a menor quantidade
		BuyerDistributionDTO minBuyerDTO = buyerDistributions.parallelStream()
				.min(Comparator.comparing(BuyerDistributionDTO::getQuantity)).orElseThrow(NoSuchElementException::new);

		// se a quantidade está igual para todos, recupera o que tem menor valor de volume
		final Integer minQuantity = minBuyerDTO.getQuantity();
		if (buyerDistributions.parallelStream()
				.allMatch(buyer -> buyer.getQuantity().equals(minQuantity))) {
			minBuyerDTO = buyerDistributions.parallelStream().min(Comparator.comparing(BuyerDistributionDTO::getVolume))
					.orElseThrow(NoSuchElementException::new);
		}
		return minBuyerDTO;
	}
	
	/**
	 * Retorna a lista de transações de acordo com o produto passado como parâmetro
	 * @param product
	 * @return Lista de transações
	 * @throws BusinessException caso não seja encontrada nenhuma transação para o produto
	 */
	private List<StockTransaction> findStockTransactionsByProduct(String product) {
		List<StockTransaction> transactions = this.stockTransactionService.findAllByProduct(product);
		
		if (transactions.isEmpty()) {
			throw new BusinessException("O produto '" + product + "' não foi encontrado.");
		}
		
		return transactions;
	}
	
}
