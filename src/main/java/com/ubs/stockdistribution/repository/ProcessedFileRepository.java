package com.ubs.stockdistribution.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ubs.stockdistribution.entity.ProcessedFile;
import com.ubs.stockdistribution.enums.ProcessedFileType;

/**
 * Repository do Spring para operações referentes à arquivos importados no sistema
 * 
 * @author douglas.guisi@gft.com
 *
 */
@Repository
public interface ProcessedFileRepository extends JpaRepository<ProcessedFile, Integer> {

	List<ProcessedFile> findAllByType(ProcessedFileType type);
}
