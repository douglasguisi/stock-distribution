package com.ubs.stockdistribution.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ubs.stockdistribution.entity.StockTransaction;

/**
 * Repository do Spring para operações referentes à transações de compra ou venda
 * 
 * @author douglas.guisi@gft.com
 *
 */
@Repository
public interface StockTransactionRepository extends JpaRepository<StockTransaction, Integer> {

	List<StockTransaction> findAllByProduct(String product);
}
