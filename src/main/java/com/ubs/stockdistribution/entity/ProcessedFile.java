package com.ubs.stockdistribution.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.ubs.stockdistribution.enums.ProcessedFileType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Entidade que representa um arquivo de transações de compra ou venda importado no sistema
 * 
 * @author douglas.guisi@gft.com
 *
 */
@Data
@Entity
@Table
@Getter @Setter @NoArgsConstructor @EqualsAndHashCode(of = "id")
public class ProcessedFile {

	@Id
	@SequenceGenerator(name = "SEQ_PROCESSED_FILE", sequenceName = "SEQ_PROCESSED_FILE", initialValue = 1, allocationSize = 1)
	@GeneratedValue(generator = "SEQ_PROCESSED_FILE", strategy = GenerationType.SEQUENCE)
	private Integer id;

	@NotNull
	@Column(length = 100)
	private String fileName;

	@Enumerated(EnumType.STRING)
	private ProcessedFileType type;
	
}
