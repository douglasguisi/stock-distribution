package com.ubs.stockdistribution.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.ubs.stockdistribution.enums.StockTransactionType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Entidade que representa uma transação de compra ou venda de um produto
 * 
 * @author douglas.guisi@gft.com
 *
 */
@Data
@Entity
@Table
@Getter @Setter @NoArgsConstructor @EqualsAndHashCode(of = "id")
public class StockTransaction {

	@Id
	@SequenceGenerator(name = "SEQ_STOCK_TRANSACTION", sequenceName = "SEQ_STOCK_TRANSACTION", initialValue = 1, allocationSize = 1)
	@GeneratedValue(generator = "SEQ_STOCK_TRANSACTION", strategy = GenerationType.SEQUENCE)
	private Integer id;

	@NotNull
	@Column(length = 50)
	private String product;

	@NotNull
	private Integer quantity;
	
	@NotNull
	private BigDecimal price;
	
	@NotNull
	@Column(length = 5)
	private String type;
	
	@NotNull
	@Column(length = 200)
	private String industry;
	
	@NotNull
	@Column(length = 2)
	private String origin;
	
	@Enumerated(EnumType.STRING)
	private StockTransactionType transactionType;
	
}
