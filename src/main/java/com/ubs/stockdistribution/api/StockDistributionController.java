package com.ubs.stockdistribution.api;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ubs.stockdistribution.dto.StockDistributionDTO;
import com.ubs.stockdistribution.service.StockDistributionService;

/**
 * Controller que define operações referentes à distribuição de estoque
 * 
 * @author douglas.guisi@gft.com
 *
 */

@RestController
public class StockDistributionController {
	
	@Autowired
	private StockDistributionService stockDistributionService;

	@GetMapping(value = "/stockDistribution", produces = { "application/json" })
	public ResponseEntity<StockDistributionDTO> calculateStockDistribution(
			@Valid @RequestParam(value = "product", required = true) String product,
			@Valid @RequestParam(value = "buyersNumber", required = true) Integer buyersNumber) {

		StockDistributionDTO dto = this.stockDistributionService.calculateStockDistribution(product, buyersNumber);
		return ResponseEntity.ok(dto);
	}
}
