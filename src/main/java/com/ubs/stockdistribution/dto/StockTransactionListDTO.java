package com.ubs.stockdistribution.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * DTO que representa uma lista de transações de compra ou venda
 * 
 * @author douglas.guisi@gft.com
 *
 */
@JsonInclude(value = Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data @Getter @Setter @NoArgsConstructor @EqualsAndHashCode
public class StockTransactionListDTO {

	private List<StockTransactionDTO> data;
	
}
