package com.ubs.stockdistribution.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ubs.stockdistribution.enums.StockTransactionType;
import com.ubs.stockdistribution.json.CurrencyDeserializer;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * DTO que representa a transação de compra ou venda de um produto
 * 
 * @author douglas.guisi@gft.com
 *
 */
@JsonInclude(value = Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data @Getter @Setter @NoArgsConstructor @EqualsAndHashCode
public class StockTransactionDTO {

	private String product;
	private Integer quantity;
	@JsonDeserialize(using = CurrencyDeserializer.class)
	private BigDecimal price;
	private String type;
	private String industry;
	private String origin;
	private StockTransactionType transactionType;
	
}
