package com.ubs.stockdistribution.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.NoSuchElementException;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * DTO que representa o cálculo de distribuição realizado para um comprador
 * 
 * @author douglas.guisi@gft.com
 *
 */
@JsonInclude(value = Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data @Getter @Setter @NoArgsConstructor @EqualsAndHashCode
public class BuyerDistributionDTO {

	private String buyerName;
	private List<BuyerDistributionItemDTO> items;
	private Integer quantity;
	private BigDecimal volume;
	private BigDecimal averagePrice;
	
	public void addItem(BuyerDistributionItemDTO itemDTO) {
		this.items.add(itemDTO);
	}
	
	public void incrementItemQuantity(Integer transactionId, int increment) {
		BuyerDistributionItemDTO item = this.items.stream().filter(dto -> dto.getTransactionId().equals(transactionId)).findFirst().orElseThrow(NoSuchElementException::new);
		item.incrementQuantity(increment);
	}
	
	public void calculateTotals() {
		this.quantity = this.items.stream().mapToInt(BuyerDistributionItemDTO::getQuantity).sum();
		this.volume = this.items.stream().map(BuyerDistributionItemDTO::getVolume).reduce(BigDecimal.ZERO, BigDecimal::add);
		this.averagePrice = this.volume.divide(BigDecimal.valueOf(this.quantity), 4, RoundingMode.HALF_UP);
	}
}
