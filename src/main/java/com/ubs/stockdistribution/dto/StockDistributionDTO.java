package com.ubs.stockdistribution.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * DTO que concentra o cálculo de distribuição realizado para uma relação de compradores
 * 
 * @author douglas.guisi@gft.com
 *
 */
@JsonInclude(value = Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data @Getter @Setter @NoArgsConstructor @EqualsAndHashCode
public class StockDistributionDTO {

	private String product;
	private Integer quantity;
	private BigDecimal volume;
	private BigDecimal averagePrice;
	private List<BuyerDistributionDTO> buyerDistributions;
	
	public void calculateTotals() {
		this.quantity = this.buyerDistributions.stream().mapToInt(BuyerDistributionDTO::getQuantity).sum();
		this.volume = this.buyerDistributions.stream().map(BuyerDistributionDTO::getVolume).reduce(BigDecimal.ZERO, BigDecimal::add);
		this.averagePrice = this.volume.divide(BigDecimal.valueOf(this.quantity), 2, RoundingMode.HALF_UP);
	}
}
