package com.ubs.stockdistribution.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * DTO que representa a distribuição de um item para um comprador
 * 
 * @author douglas.guisi@gft.com
 *
 */
@JsonInclude(value = Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data @Getter @Setter @EqualsAndHashCode
public class BuyerDistributionItemDTO {

	@JsonIgnore
	private Integer transactionId;
	private Integer quantity;
	private BigDecimal price;
	private BigDecimal volume;
	
	public BuyerDistributionItemDTO(Integer transactionId, Integer quantity, BigDecimal price) {
		this.transactionId = transactionId;
		this.quantity = quantity;
		this.price = price;
		this.calculateVolume();
	}
	
	public void incrementQuantity(int increment) {
		this.quantity += increment;
		this.calculateVolume();
	}
	
	private void calculateVolume() {
		this.volume = BigDecimal.valueOf(quantity).multiply(price);
	}
}
