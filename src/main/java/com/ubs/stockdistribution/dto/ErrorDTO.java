package com.ubs.stockdistribution.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * DTO utilizado para retorno de erros de negócio ou exceções de sistema
 * 
 * @author douglas.guisi@gft.com
 *
 */
@JsonInclude(value = Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data @Getter @Setter @NoArgsConstructor @EqualsAndHashCode
public class ErrorDTO {

	private Integer code;
	private String message;
	
}
