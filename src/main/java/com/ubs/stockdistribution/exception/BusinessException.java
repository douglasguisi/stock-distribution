package com.ubs.stockdistribution.exception;

/**
 * Exceção de negócio genérica lançada quando uma regra de negócio é violada
 * 
 * @author douglas.guisi@gft.com
 *
 */
public class BusinessException extends RuntimeException {

	private static final long serialVersionUID = -3996654114711364427L;
	
	public BusinessException(String msg) {
		super(msg);
	}

	public BusinessException(String msg, Throwable e) {
		super(msg, e);
	}
}
