package com.ubs.stockdistribution.exception;

import java.text.MessageFormat;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.ubs.stockdistribution.dto.ErrorDTO;

/**
 * Handler de exceptions para que as exceptions lançadas sejam retornadas seguindo o contrato de {@link ErrorDTO}
 * 
 * @author douglas.guisi@gft.com
 *
 */
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler(value = { Exception.class })
	protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
		logger.error(ex.getMessage(), ex);
		return buildResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
	}
	
	@ExceptionHandler(value = { IllegalArgumentException.class })
	protected ResponseEntity<Object> handleIllegalArgument(IllegalArgumentException ex, WebRequest request) {
		logger.error(ex.getMessage(), ex);
		return buildResponseEntity(HttpStatus.BAD_REQUEST, ex.getMessage());
	}
	
	@ExceptionHandler(value = BusinessException.class)
	protected ResponseEntity<Object> handleApiException(BusinessException ex, WebRequest request) {
		return buildResponseEntity(HttpStatus.BAD_REQUEST, ex.getMessage());
	}

	@ExceptionHandler(value = ConstraintViolationException.class)
	protected ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {
		Set<ConstraintViolation<?>> violations = ex.getConstraintViolations();
		List<String> details = violations.stream().map(ConstraintViolation::getMessage).collect(Collectors.toList());
		return buildResponseEntity(HttpStatus.BAD_REQUEST, StringUtils.join(details, " "));
	}

	@Override
	protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		return buildResponseEntity(HttpStatus.BAD_REQUEST, MessageFormat.format("O campo {0} é obrigatório", ex.getParameterName()));
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<ObjectError> errors = ex.getBindingResult().getAllErrors();
		List<String> details = errors.stream().map(ObjectError::getDefaultMessage).collect(Collectors.toList());

		return buildResponseEntity(HttpStatus.BAD_REQUEST, StringUtils.join(details, " "));
	}

	private ResponseEntity<Object> buildResponseEntity(HttpStatus httpStatus, String message) {
		ErrorDTO error = new ErrorDTO();
		error.setCode(httpStatus.value());
		error.setMessage(message);
		return new ResponseEntity<>(error, httpStatus);
	}
}